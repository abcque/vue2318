import {createApp} from 'vue'
import './style.css'
import App from './App.vue'

// 导入使用的插件
import myPlugin from "./plugin/index.js";

let app = createApp(App)

app.directive(
    'highlight',
    {
        mounted(el, binding, vNode, preNode) {
            // 指令所在的元素
            console.log(el);

            console.log(binding);

            let delay = 0       // 闪烁的延迟时间

            // 通过修饰符判断是否存在延迟
            if (binding.modifiers.delayed) {
                delay = 3000
            }

            // 通过修饰符确定是否进行闪烁
            if (binding.modifiers.blink) {
                let mainColor = binding.value.mainColor
                let secondColor = binding.value.secondColor
                let currentColor = mainColor

                // 在达到延迟时间后开始进行闪烁
                setTimeout(
                    () => {
                        setInterval(
                            () => {
                                currentColor === secondColor ? (currentColor = mainColor) : (currentColor = secondColor)

                                if (binding.arg === 'background') {
                                    el.style.backgroundColor = currentColor
                                } else {
                                    el.style.color = currentColor
                                }

                            },
                            binding.value.delay,
                        )
                    },
                    delay,
                )
            } else {
                setTimeout(
                    () => {
                        if (binding.arg === 'background') {
                            el.style.backgroundColor = binding.value.mainColor
                        } else {
                            el.style.color = binding.value.mainColor
                        }
                    },
                    delay
                )
            }


        }
    },
)


app.directive(
    'color',
    (el, binding, vNode, preNode) => {
        el.style.color = binding.value
    }
)

app.directive(
    'permission',
    (el, binding, vNode, prevVNode) => {
        // console.log(binding);
        let {arg, modifiers, value} = binding

        // 定义处理用户的角色
        let userRole = ['admin']

        // 处理用户的参数，传入的是一个非空数组
        if (value && value instanceof Array && value.length > 0) {
            // 判断 userRole 是否包含 value 中的内容
            const hasPermission = userRole.some(role => value.includes(role))

            // 不包含权限
            if (!hasPermission) {
                // 原始是否存在父元素，存在的话，利用父元素将该元素删除
                el.parentNode && el.parentNode.removeChild(el)
            } else {

            }
        } else {
            throw new Error("请按照字符串的格式设置参数， v-permission=['admin', 'editor']")
        }
    }
)

app.use(myPlugin, {
    small: 12,
    medium: 14,
    large: 36,
})




app.mount('#app')
