const myPlugin = {
    install(app, options) {
        console.log(app);
        console.log(options);

        // 配置一个全局的方法， globalMethod 为自定义的全局函数名称


        // 注册一个全局的组件，在 App.vue 中不导入，也可以使用
        // app.component("Header", Header)

        // 注册一个自定义指令
        app.directive(
            'upper',
            function (el, binding, vNode, prevVNode) {
                el.textContent = binding.value.toUpperCase()

                if (binding.arg === 'small') {
                    el.style.fontSize = options.small + "px"
                }
                else if(binding.arg === 'medium') {
                    el.style.fontSize = options.medium + "px"
                }
                else {
                    el.style.fontSize = options.large + "px"
                }
            }
        )
    }
}

export default myPlugin;