任务一、在 HTML 页面中有一个文本输入框和标签，要求通过原生的 Javascript 实现将输入框中的内容在发生变化的时候，自动将内容显示到标签中。

    <label for="name">姓名</label><input type="text" name="name" id="name">
    <label id="content"></label>

任务二：在任务一的基础上修改代码，要求在 JavaScript 设置 name 的初始值，并将其显示到输入框中，在输入框内容发生变化的时，自动将内容显示到 标签中。
        <form action="">
            <label for="name">姓名</label><input type="text" name="name" id="name" data-bind:value="name">
            <label id="content"></label>
        </form>

        let name = "abc"


任务三：在任务二的基础上，使用 Vue 实现相同的功能.


任务四：在 HTML 页面上，添加一些复选框，并通过原生 Javascript获取复选框的值，然后在控制台打印。

任务五：在任务四的基础上，将选中的信息在 页面上进行显示。