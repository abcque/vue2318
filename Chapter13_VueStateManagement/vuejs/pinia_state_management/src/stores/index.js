import {createPinia} from "pinia";
import piniaPluginPersistedState from "pinia-plugin-persistedstate"

let pinia = createPinia();

pinia.use(piniaPluginPersistedState)
export default pinia