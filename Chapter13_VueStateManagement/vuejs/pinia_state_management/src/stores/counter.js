import {defineStore} from "pinia";

let useCounterStore = defineStore(
    "counter",
    {
        // 定义状态变量
        state: () => {
            return {
                count: 0
            }
        },

        getters: {
            oddOrEven() {
                return Math.abs(this.count) % 2 === 1 ? "奇数" : "偶数"
            }
        },
        actions: {
            increment(number) {
                this.count += number
            },
            decrement(number) {
                this.count -= number
            },
            incrementIfOdd(number) {
                if (this.count % 2 === 1) {
                    this.count += number
                }
            },

            incrementAsync(number) {
                setTimeout(
                    () => {
                        this.increment(number)
                    },
                    5000
                )
            }
        },
        persist: true,
    }
);

export default useCounterStore;

