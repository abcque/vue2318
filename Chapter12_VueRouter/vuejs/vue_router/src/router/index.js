import {createRouter, createWebHashHistory, createWebHistory} from "vue-router";

import  NProgress from "nprogress"
import "nprogress/nprogress.css"

/*import Home from "../components/Home.vue";
import User from "../components/User.vue";
import Article from "../components/Article.vue";
import News from "../components/News.vue";
import Student from "../components/Student.vue";
import Profile from "../components/Profile.vue";
import Post from "../components/Post.vue";
import Content from "../components/Content.vue";
import Header from "../components/Header.vue";
import Footer from "../components/Footer.vue";
import NotFound from "../components/NotFound.vue";
import Default from "../components/Default.vue";
import ServerError from "../components/ServerError.vue";
import ContainNumber from "../components/ContainNumber.vue";*/

const Home = ()=> import("../components/Home.vue")
const User = ()=> import("../components/User.vue")
const Article = ()=> import("../components/Article.vue")
const News = ()=> import("../components/News.vue")
const Student = ()=> import("../components/Student.vue")
const Profile = ()=> import("../components/Profile.vue")
const Post = ()=> import("../components/Post.vue")
const Content = ()=> import("../components/Content.vue")
const Header = ()=> import("../components/Header.vue")
const Footer = ()=> import("../components/Footer.vue")
const NotFound = ()=> import("../components/NotFound.vue")
const Default = ()=> import("../components/Default.vue")
const ServerError = ()=> import("../components/ServerError.vue")
const ContainNumber = ()=> import("../components/ContainNumber.vue")


const routes = [
    {
        path: "/",
        component: Home,
        name: "home",
    },
    {
        path: "/user",
        component: User,
        name: "user"
    },
    {
        path: "/article/:id",
        component: Article,
        name: "article",
    },
    {
        path: "/news/:id",
        component: News,
        name: "news",
    },
    {
        path: "/student/:name",
        component: Student,
        name: 'student',
        children:
            [
                {
                    path: "profile",
                    component: Profile,
                    name: 'profile'
                },
                {
                    path: "post",
                    component: Post,
                    name: 'post',
                },
            ],

    },
    {
        path: "/home",
        redirect: '/',
        name: 'redi'
    },
    {
        path: "/product/:id",
        // component: Content,
        components: {
            "default": Content,
            "router-view-header": Header,
            "router-view-footer": Footer,
        }
    },
/*    {
        path: "/:pathMatch([^0-9]*)",
        component: Default,
        name: "default",
    },
    {
        path: "/:pathMatch(4[0-9]*)",
        redirect: '/404',
    },
    {
        path: "/:pathMatch(5[0-9]*)",
        component: ServerError,
        name: "serverError",
    },
    {
        path: "/:pathMatch(.*?[0-9]*?.*)",
        component: ContainNumber,
        name: "containNumber",
    },*/
    {
        path: "/404",
        component: NotFound,
        name: "notfound",
    },
    {
        path: "/:notFound(.*)",
        redirect: '/404',
    }
]

let router = createRouter({
    history: createWebHistory(),
    routes: routes,
});


router.beforeEach((to, from, next)=>{
    // 开启进度条
    NProgress.start()

    setTimeout(
        ()=>{

        },
        3000
    )

    // 读取处理的数据类型为 String
    let status = localStorage.getItem("logged");

    if (to.path !== "/") {
        if (status === true.toString()) {
            next()
        }
        else {
            next({path:"/"})
        }
    }
    else
        next()
})

router.afterEach(()=>{
    NProgress.done()
})


export default router