import {createApp, ref} from "../../../../js/vue.esm-browser.js";

let app = createApp(
    {
        setup() {
            let musics = ref(
                [
                    {
                        id: 1,
                        name: "不再犹豫",
                        author: "Beyond",
                        src: "../static/不再犹豫.mp3",
                    },
                    {
                        id: 2,
                        name: "光辉岁月",
                        author: "Beyond",
                        src: "../static/光辉岁月.mp3",
                    },
                    {
                        id: 3,
                        name: "不再犹豫",
                        author: "Beyond",
                        src: "../static/海阔天空.mp3",
                    },

                ]
            )
            return {musics}
        }
    }
);

app.mount("#app")