import {createApp, ref, reactive, computed} from "../../../../js/vue.esm-browser.js";

let app = createApp(
    {
        setup() {
            let musics = reactive(
                [
                    {
                        id: 1,
                        name: "不再犹豫",
                        author: "Beyond",
                        source: "../static/不再犹豫.mp3",
                    },
                    {
                        id: 2,
                        name: "光辉岁月",
                        author: "Beyond",
                        source: "../static/光辉岁月.mp3",
                    },
                    {
                        id: 3,
                        name: "不再犹豫",
                        author: "Beyond",
                        source: "../static/海阔天空.mp3",
                    },
                ]
            )

            let currentIndex = ref(0)

            let songSrc = computed(
                () => {
                    return musics[currentIndex.value].source
                }
            )

            function changeMusic(index) {
                // console.log(musics[index]);
                // songSrc.value = musics[index].source
                currentIndex.value = index
                localStorage.setItem("currentSongIndex", index)
            }

            // 返回的数值为 string 或 null
            let item = localStorage.getItem("currentSongIndex");
            if (item === null) {
                currentIndex.value = 0
            } else {
                currentIndex.value = parseInt(item)
            }

            return {
                musics, songSrc, changeMusic, currentIndex
            }
        }
    }
);

app.mount("#app")