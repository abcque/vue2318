import {createApp, reactive, ref, computed} from "../../../js/vue.esm-browser.js";

let app = createApp(
    {
        setup() {
            let books = reactive(
                [
                    {
                        id: 1,
                        name: "Vue 从入门到精通",
                        date: '2022-12-20',
                        price: 128.99,
                        author: 'abcque',
                        count: 1,
                    },
                    {
                        id: 2,
                        name: "JavaScript 入门经典",
                        date: '2018-06-10',
                        price: 168.29,
                        author: 'James',
                        count: 1,
                    },
                    {
                        id: 3,
                        name: "Servlet 源码详解",
                        date: '2012-03-28',
                        price: 45.99,
                        author: 'Oracle',
                        count: 1,
                    },
                    {
                        id: 4,
                        name: "Java 程序设计语言",
                        date: '2024-09-12',
                        price: 81.29,
                        author: 'Sun',
                        count: 1,
                    },
                    {
                        id: 5,
                        name: "Http + CSS 网页设计",
                        date: '2020-11-28',
                        price: 92.29,
                        author: 'Mozilla',
                        count: 1,
                    },
                ]
            )

            let total = computed(
                () => {
                    let sum = 0
                    for (let i = 0; i < books.length; i++) {
                        sum += books[i].price * books[i].count
                        // console.log(sum);
                    }

                    return sum.toFixed(2)
                }
            )

            /* function total() {
                 let sum = 0
                 for (let i = 0; i < books.length; i++) {
                     sum += books[i].price * books[i].count
                     console.log(sum);
                 }
                 console.log(sum);
                 return sum
             }*/
            return {
                books, total
            }
        }
    }
);

app.mount("#app")