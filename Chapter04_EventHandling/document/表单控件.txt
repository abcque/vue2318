form
    action  指定表单提交的页面，
    method  提交方式。get/post


input
    type   text/password/submit/check
    id  标识控件的唯一
    name 指定控件的名称，在提交到服务器的时候使用
    value   显示的内容
    placeholder 占位符，提示信息
    autocomplete 自动完成输入

label
    for 和 input 控件 id 组合绑定，关联到一起



